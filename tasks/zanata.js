/*
 * grunt-zanata-js
 * https://bitbucket.org/tagoh/grunt-zanata-js
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

const _ = require('underscore');
const ZanataClient = require('zanata-js').ZanataClient;
const path = require('path');
const co = require('co');

function getVersion(grunt, options) {
  return new Promise((resolve, reject) => {
    if (typeof options.version === 'function') {
      let ret = options.version();
      if (typeof ret === 'string') {
        resolve(ret);
      } else if (ret instanceof Promise) {
        ret
          .then(resolve, reject);
      } else {
        reject(new Error('Unknown object type in version: ' + typeof ret));
      }
    } else if (typeof options.version === 'string') {
      resolve(options.version);
    } else if (typeof options['project-version'] === 'string') {
      resolve(options['project-version'])
    } else {
      reject(new Error('Unable to determine the version from options'));
    }
  });
}

function doDirs(grunt, options, mode, dirs) {
  return co(function* () {
    let result = [];

    for (let i = 0; i < dirs.length; i++) {
      let fileObj = dirs[i].obj;
      let v = dirs[i].dirs[0];

      result.push(yield new Promise((resolve, reject) => {
        getVersion(grunt, options)
          .then((ver) => {
            let zp = new ZanataClient.Project(options)
                .on('fail', (e) => reject(e));

            if (mode === 'pull') {
              zp
                .on('data_pull', (d) => {
                  if (d.type === 'pot') {
                    let file = path.join(v, d.name + '.pot');

                    grunt.file.write(file, d.data);
                    grunt.log.ok('Downloaded into ' + file);
                  } else if (d.type === 'po') {
                    let fn = new ZanataClient.FileMappingRule(zp.config.get('rules'))
                        .getPath(zp.config.get('project-type'),
                                 {
                                   path: v,
                                   locale: d.locale,
                                   filename: d.name,
                                   extension: 'po'
                                 });

                    grunt.file.write(fn, d.data);
                    grunt.log.ok('Downloaded into ' + fn);
                  } else {
                    reject(new Error('Unknown file type: ' + d.type));
                  }
                })
                .on('end_pull', (r) => resolve(r));
              if (fileObj.docId) {
                if (fileObj.type === 'source') {
                  zp.pullSource(options.project, ver, fileObj.docId,
                                {verbose: options.verbose || false,
                                 locales: fileObj.locales,
                                 skeletons: options.skeletons || false,
                                 potdir: v});
                } else if (fileObj.type === 'trans' || fileObj.type === undefined) {
                  zp.pullTranslation(options.project, ver, fileObj.docId, fileObj.locales,
                                     {verbose: options.verbose || false,
                                      skeletons: options.skeletons || false,
                                      potdir: v,
                                      podir: v});
                } else if (fileObj.type === 'both') {
                  reject(new Error('Not yet supported. Use separate entries for source and trans so far.'));
                } else {
                  reject(new Error('Unknown pull type: ' + fileObj.type));
                }
              } else {
                zp
                  .pull({
                    project: options.project,
                    version: ver,
                    pullType: fileObj.type,
                    verbose: options.verbose || false,
                    locales: fileObj.locales,
                    skeletons: options.skeletons || false,
                    potdir: v,
                    podir: v
                  });
              }
            } else if (mode === 'push') {
              zp
                .on('data_push', (fn) => grunt.log.ok('Uploaded ' + fn))
                .on('end_push', (r) => resolve(r))
                .push({project: options.project,
                       version: ver,
                       pushType: fileObj.type,
                       verbose: options.verbose || false,
                       locales: fileObj.locales,
                       potdir: v,
                       podir: v
                      });
            } else {
              grunt.fail.warn('Invalid mode: ' + mode);
              resolve(false);
            }
          })
          .catch((e) => reject);
      }));
    }
    return result;
  });
}

function doPot(grunt, options, mode, potfiles) {
  return co(function* () {
    let result = [];

    for (let i = 0; i < potfiles.length; i++) {
      let v = potfiles[i];
      result.push(yield new Promise((resolve, reject) => {
        getVersion(grunt, options)
          .then((ver) => {
            let zp = new ZanataClient.Project(options)
                .on('fail', (e) => reject(e));

            if (mode === 'pull') {
              zp
                .on('data_pull', (d) => {
                  grunt.file.write(v, d.data);
                  grunt.log.ok('Downloaded into ' + v);
                })
                .on('end_pull', (r) => resolve(r))
                .pullSource(options.projecct,
                            ver,
                            path.basename(v, '.pot'),
                            {verbose: options.verbose || false,
                             potdir: path.dirname(v)});
            } else if (mode === 'push') {
              zp
                .on('data_push', (fn) => {
                  grunt.log.ok('Uploaded ' + fn);
                })
                .on('end_push', (r) => resolve(r))
                .pushSource(options.project,
                            ver,
                            path.basename(v, '.pot'),
                            {verbose: options.verbose || false,
                             potdir: path.dirname(v)});
            } else {
              grunt.fail.warn('Invalid mode: ' + mode);
              resolve(false);
            }
          })
          .catch((e) => reject);
      }));
    }
    return result;
  });
}

module.exports = function(grunt) {

  // Please see the grunt documentation for more information regarding task
  // creation: https://github.com/gruntjs/grunt/blob/devel/docs/toc.md

  grunt.registerMultiTask('zanata', 'Perform operations from/to Zanata', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    let options = this.options({});
    let self = this;
    let potfiles = [];
    let pofiles = [];
    let dirs = [];
    let mode = this.target.split('_')[0];
    let done = self.async();

    if (mode === 'createVersion') {
      getVersion(grunt, options)
        .then((ver) => {
          new ZanataClient.Project(options)
            .on('fail', (e) => {
              if (options.ignore_error_if_exists && /already exists/.test(e.message)) {
                // ignore error
                grunt.log.write(e + '\n');
                done(true);
              } else {
                grunt.log.error(e);
                done(false);
              }
            })
            .on('data_create', (d) => {
              grunt.log.ok(d);
              done(true);
            })
            .createVersion(options.project, ver, options);
        })
        .catch((e) => {
          grunt.log.error(e);
          done(false);
        });
    } else {
      // Iterate over all specified file groups.
      this.files.forEach((fileObj) => {
        // The source files to be concatenated. The "nonull" option is used
        // to retain invalid files/patterns so they can be warned about.
        let gfiles = grunt.file.expand({nonull: true}, fileObj.src);
        let ff = (extname) => {
          return (fn) => {
            if (!grunt.file.exists(fn)) {
              grunt.log.error('Source file "' + fn + '" not found.');
              return false;
            }
            let ext = path.extname(fn);
            return ext === extname ? true : false;
          };
        };
        potfiles.push(gfiles.filter(ff('.pot')));
        pofiles.push(gfiles.filter(ff('.po')));
        let dd = gfiles.filter((fn) => grunt.file.isDir(fn) ? true : false);
        dirs.push({obj: fileObj, dirs: dd});
      });
      potfiles = _.flatten(potfiles);
      pofiles = _.flatten(pofiles);
      dirs = dirs.filter((o) => o.dirs.length > 0);

      if (pofiles.length !== 0) {
        grunt.fail.warn("Performing certain translation files only isn't supported");
        done(false);
      }
      if (potfiles.length === 0 && dirs.length === 0) {
        grunt.fail.warn('Source file(s) "' + gfiles.join(',') + '" is not a POT nor PO file');
        done(false);
      }

      Promise.all([

        (() => {
          if (dirs.length > 0)
            return doDirs(grunt, options, mode, dirs);
          else
            return Promise.resolve(true);
        })(),
        (() => {
          if (potfiles.length > 0)
            return doPot(grunt, options, mode, potfiles);
          else
            return Promise.resolve(true);
        })()])
        .then((v) => done(true))
        .catch((e) => {
          grunt.log.error(e);
          done(false);
        });
    }
  });
};
