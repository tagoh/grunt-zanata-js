/*
 * grunt-zanata-js
 * https://bitbucket.org/tagoh/grunt-zanata-js
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

const nodeGit = require('nodegit');
const co = require('co');

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: ['Gruntfile.js', 'tasks/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    zanata: {
      push_dir_sample: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'testing',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source'}, // upload only POT files
          {src: 'test/fixtures/po', type: 'trans'},  // upload only PO files
        ]
      },
      pull_dir_docid: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'testing',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source', docId: 'test'}, // download only POT files
          {src: 'test/fixtures/po', type: 'trans', docId: 'test'} // download only PO files
        ]
      },
      pull_sample: {
        // options is simply passed to ZanataClient.Config
        // of course if you have $XDG_CONFIG_HOME/zanata.ini
        // it will be used as a base config.
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'testing',
          'project-type': 'gettext'
        },
        files: [
          {src: 'test/fixtures/test.pot'},
//          {src: 'test/fixtures/test.po'}, // not supported
        ]
      },
      pull_sample_func: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-type': 'gettext',
          version: (() => {
            return co(function*() {
              return yield nodeGit.Repository.open('.')
                .then((repo) => {
                  return repo.getCurrentBranch()
                    .then((ref) => {
                      return nodeGit.Branch.name(ref)
                        .then((s) => {
                          // 'testing' version only available on Zanata repo...
                          if (s === 'master')
                            return 'testing';
                        });
                    });
                });
            });
          }),
        },
        files: [
          {src: 'test/fixtures/test.pot'},
        ]
      },
      pull_dir_sample: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'testing',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source'}, // download only POT files
          {src: 'test/fixtures'}, // download both POT and PO files
          {src: 'test/fixtures', type: 'trans'} // download only PO files
        ]
      },
      createVersion_sample: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          version: 'testing',
          'project-type': 'Gettext',
          ignore_error_if_exists: true
        }
      },
      createVersion_func: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-type': 'Gettext',
          ignore_error_if_exists: true,
          version: (() => {
            return co(function*() {
              return yield nodeGit.Repository.open('.')
                .then((repo) => {
                  return repo.getCurrentBranch()
                    .then((ref) => {
                      return nodeGit.Branch.name(ref)
                        .then((s) => s);
                    });
                });
            });
          })
        }
      },
      // wrong example; test case to see an exception
      /*
      createVersion_emptyVersion: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-type': 'Gettext'
        }
      },
      createVersion_emptyVersionFunc: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-type': 'Gettext',
          version: (() => new Promise((a,b)=>{b(true);}))
        }
      },
      */
      createVersion_ignoreError: {
        options: {
          url: 'https://translate.zanata.org',
          project: 'grunt-zanata-js',
          'project-type': 'Gettext',
          version: 'testing',
          ignore_error_if_exists: true
        }
      }
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-readme');

  grunt.registerTask('test', ['clean', 'zanata']);
  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'repos', 'readme', 'test']);
};
